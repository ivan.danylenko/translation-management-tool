require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
javascripts_dir = "js"

output_style = :expanded # :expanded or :nested or :compact or :compressed
$(document).ready(function() {
    var inputType = "textarea";
	$('.confirmable').click(function(event) {
		if (!confirm('Are you sure?')) {
			event.preventDefault();
		}
	});

	var newRows = 0;

    function updateNewRows() {
        $('#new-rows-counter').html(newRows);
    }

    $('#translations-add-row').click(function(event) {
        event.preventDefault();
        newRows++;
        var newRow = $('#empty-translations-row').clone();
        $(newRow).attr('id', 'translation-key-new_row_' + newRows).find(inputType).each(function() {
            $(this).attr('name', $(this).attr('name').replace('__NEW_ROW__', 'new_row_' + newRows));
            $(this).attr('data-key', $(this).attr('data-key').replace('__NEW_ROW__', 'new_row_' + newRows));
        });
        $('#translations-rows').append(newRow);
        $('#translations-rows').find('.no-translations-row').remove();

        updateNewRows();
    });

    function toggleDeletedRow(key) {
        $('#translation-key-' + key).toggleClass('danger');

        if ($('#translation-key-' + key).hasClass('danger')) {
            $('#removed-keys-list').append('<input type="hidden" name="translationsToRemove[]" value="' + key + '" id="translation-key-to-remove-' + key + '" />');
            $('#translation-key-' + key).find(inputType).attr('disabled', true);
            $('#translation-key-' + key).find('input[type=checkbox]').attr('disabled', true);
            $('#translation-key-' + key).find('.translation-to-translate-container').hide();
        } else {
            $('#removed-keys-list').find('#translation-key-to-remove-' + key).remove();
            $('#translation-key-' + key).find(inputType).attr('disabled', false);
            $('#translation-key-' + key).find('input[type=checkbox]').attr('disabled', false);
            $('#translation-key-' + key).find('.translation-to-translate-container').show();
        }
        $('#translation-key-' + key).find('.button-remove, .button-keep').toggleClass('hidden');
    }

    $('.translation-table').on('click', '.button-remove, .button-keep', function(e) {
        e.preventDefault();
        var key = $(this).closest('tr').find('textarea').data('key');

        toggleDeletedRow(key);
    });

    $('.tooltip-btn').tooltip();

    $('.toggle-needs-translation').on('click', function() {
        var checkBoxId = $(this).data('checkbox-id');

        $('#' + checkBoxId).attr('checked', !$('#' + checkBoxId).attr('checked'));

        $(this).toggleClass('active');
    })
});
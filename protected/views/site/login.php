<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
    'Login',
);
?>

<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
        )); ?>
        <fieldset>
            <legend>Login</legend>
            <?php 
                echo $form->textFieldControlGroup($model, 'username');
                echo $form->passwordFieldControlGroup($model, 'password');
                echo $form->checkBoxControlGroup($model, 'rememberMe');
                echo TbHtml::formActions(array(
                    TbHtml::submitButton('Login', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                ));
            ?>
        </fieldset>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->

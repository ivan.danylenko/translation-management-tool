<?php
    $this->pageTitle = Yii::t('main', 'Error');

    echo TbHtml::pageHeader(Yii::app()->name, Yii::t('main', 'Oh snap'));

    echo TbHtml::heroUnit(
        Yii::t('main', 'Oh snap!'),
        $this->renderpartial('_error_message', array('error' => $error), true)
    );


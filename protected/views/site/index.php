    <header role="banner" id="gt-header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <a href="#" class="js-gt-nav-toggle gt-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span class="octicon octicon-three-bars"></span></a>
                    <a class="navbar-brand logo" href="/"><?php echo CHtml::image('/images/logo.png', CHtml::encode(Yii::app()->name)); ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#" data-nav-section="home"><span><?php echo Yii::t('main', 'Home'); ?></span></a></li>
                        <li><a href="#" data-nav-section="about"><span><?php echo Yii::t('main', 'About'); ?></span></a></li>
                        <li><a href="#" data-nav-section="features"><span><?php echo Yii::t('main', 'Features'); ?></span></a></li>
                        <li><a href="#" data-nav-section="pricing"><span><?php echo Yii::t('main', 'Pricing'); ?></span></a></li>
                        <li><a href="#" data-nav-section="info"><span><?php echo Yii::t('main', 'Info'); ?></span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <?php echo TbHtml::heroUnit(
        Yii::t('main', 'New Translating Experience!'),
        $this->renderpartial('_jumbotron', array(), true),
        array(
            'class' => 'text-center jumbotron-primary',
            'data-section' => 'home'
        )
    ); ?>

    <div id="gt-about" data-section="about">
        <div class="container">
            <div class="row" id="about-us">
                <div class="col-md-12 section-heading text-center">
                    <?php echo CHtml::tag('h2', array('class' => 'text-center to-animate'), Yii::t('main', 'About')); ?>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 to-animate">
                            <?php echo CHtml::tag('h3', array(), Yii::t('main', 'Go Translating is the modern translation management tool. Using it, you don\'t have to feel pain every time trying to remember changes you make to your translations.')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="gt-features" data-section="features">
        <div class="container">
            <div class="row row-bottom-padded-sm">
                <div class="col-md-12 section-heading text-center">
                    <?php echo CHtml::tag('h2', array('class' => 'text-center to-animate'), Yii::t('main', 'What\'s inside')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="box to-animate">
                        <?php echo CHtml::tag('div', array('class' => 'icon colored-1'), TbHtml::icon('glyphicon-time')); ?>
                        <?php echo CHtml::tag('h3', array(), Yii::t('main', 'Changes History')); ?>
                        <?php echo CHtml::tag('p', array(), Yii::t('main', 'All the history is kept so that you can see the changes at any moment of time.')); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box to-animate">
                        <?php echo CHtml::tag('div', array('class' => 'icon colored-2'), TbHtml::icon('glyphicon-duplicate')); ?>
                        <?php echo CHtml::tag('h3', array(), Yii::t('main', 'Copy files')); ?>
                        <?php echo CHtml::tag('p', array(), Yii::t('main', 'Copy any version of a file instead of manually copying every word.')); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box to-animate">
                        <?php echo CHtml::tag('div', array('class' => 'icon colored-3'), CHtml::tag('span', array('class' => 'octicon octicon-paintcan'))); ?>
                        <?php echo CHtml::tag('h3', array(), Yii::t('main', 'Themes')); ?>
                        <?php echo CHtml::tag('p', array(), Yii::t('main', 'Multiple application themes let you pick a perfect color for you.')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="gt-pricing" data-section="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <?php echo CHtml::tag('h2', array('class' => 'text-center to-animate'), Yii::t('main', 'Pricing')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                    <div class="price-box to-animate">
                        <div class="price"><?php echo Yii::t('main', 'Free'); ?></div>
                        <hr>
                        <p><?php echo Yii::t('main', 'You can use the service for free. Here is what you get:'); ?></p>
                        <ul class="pricing-info">
                            <li><b><?php echo Yii::t('main', 'Unlimited');?></b> <?php echo Yii::t('main', 'Projects'); ?></li>
                            <li><b><?php echo Yii::t('main', 'Unlimited');?></b> <?php echo Yii::t('main', 'Files'); ?></li>
                            <li><b><?php echo Yii::t('main', 'Unlimited');?></b> <?php echo Yii::t('main', 'Languages'); ?></li>
                        </ul>
                        <a href="#" class="btn btn-success btn-md"><?php echo Yii::t('main', 'Get started'); ?></a>
                    </div>
                </div>
            <?php /* ?>
                <div class="col-md-3 col-sm-6">
                    <div class="price-box to-animate">
                        <h2 class="pricing-plan"><?php echo Yii::t('main', 'Starter'); ?></h2>
                        <div class="price"><?php echo Yii::t('main', 'Free'); ?></div>
                        <hr>
                        <ul class="pricing-info">
                            <li>1 <?php echo Yii::t('main', 'Project'); ?></li>
                            <li>2 <?php echo Yii::t('main', 'Files / project'); ?></li>
                            <li>3 <?php echo Yii::t('main', 'Languages'); ?></li>
                            <li>200 <?php echo Yii::t('main', 'Rows / file'); ?></li>
                        </ul>
                        <a href="#" class="btn btn-success btn-md"><?php echo Yii::t('main', 'Get started'); ?></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="price-box to-animate">
                        <h2 class="pricing-plan"><?php echo Yii::t('main', 'Regular'); ?></h2>
                        <div class="price"><sup class="currency">$</sup>9<small>/<?php echo Yii::t('main', 'month'); ?></small></div>
                        <hr>
                        <ul class="pricing-info">
                            <li>5 <?php echo Yii::t('main', 'Projects'); ?></li>
                            <li>10 <?php echo Yii::t('main', 'Files / project'); ?></li>
                            <li>10 <?php echo Yii::t('main', 'Languages'); ?></li>
                            <li>1000 <?php echo Yii::t('main', 'Rows / file'); ?></li>
                        </ul>
                        <a href="#" class="btn btn-success btn-md"><?php echo Yii::t('main', 'Get started'); ?></a>
                    </div>
                </div>
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-3 col-sm-6 to-animate">
                    <div class="price-box popular">
                        <div class="popular-text"><?php echo Yii::t('main', 'Best value'); ?></div>
                        <h2 class="pricing-plan"><?php echo Yii::t('main', 'Plus'); ?></h2>
                        <div class="price"><sup class="currency">$</sup>19<small>/<?php echo Yii::t('main', 'month'); ?></small></div>
                        <hr>
                        <ul class="pricing-info">
                            <li>20 <?php echo Yii::t('main', 'Project'); ?></li>
                            <li>100 <?php echo Yii::t('main', 'Files / project'); ?></li>
                            <li>25 <?php echo Yii::t('main', 'Languages'); ?></li>
                            <li>5000 <?php echo Yii::t('main', 'Rows / file'); ?></li>
                        </ul>
                        <a href="#" class="btn btn-success btn-md"><?php echo Yii::t('main', 'Get started'); ?></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="price-box to-animate">
                        <h2 class="pricing-plan"><?php echo Yii::t('main', 'Enterprise'); ?></h2>
                        <div class="price"><sup class="currency">$</sup>29<small>/<?php echo Yii::t('main', 'month'); ?></small></div>
                        <hr>
                        <ul class="pricing-info">
                            <li>Unlimited <?php echo Yii::t('main', 'Project'); ?></li>
                            <li>Unlimited <?php echo Yii::t('main', 'Files / project'); ?></li>
                            <li>Unlimited <?php echo Yii::t('main', 'Languages'); ?></li>
                            <li>Unlimited <?php echo Yii::t('main', 'Rows / file'); ?></li>
                        </ul>
                        <a href="#" class="btn btn-success btn-md"><?php echo Yii::t('main', 'Get started'); ?></a>
                    </div>
                </div>
            <?php */ ?>

            </div>
        </div>
    </div>

    <div id="gt-info" data-section="info">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center to-animate">
                    <?php echo CHtml::tag('h2', array('class' => 'words-count single-animate animate-info-1'), $countPhrases ); ?>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext single-animate animate-info-2">
                            <?php echo CHtml::tag('h3', array('class' => 'words-count-description'), Yii::t('main', 'This is how many phrases are currently in the application.')); ?>
                                <?php
                                    echo CHtml::openTag('p');
                                        echo TbHtml::link(Yii::t('main', 'Sign up'), '#') . ' ' .
                                        Yii::t('main', 'or') . ' ' .
                                        TbHtml::link(Yii::t('main', 'Login'), array('site/login')) . ' ' .
                                        Yii::t('main', 'to make this world more translated.');
                                    echo CHtml::closeTag('p');
                                ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer id="gt-footer" role="contentinfo">
        <div class="container">
            <div class="row row-bottom-padded-sm">
                <div class="col-md-6 copyright text-left">
                    <?php echo '&copy; ' . date('Y') . ' ' . TbHtml::link(CHtml::encode(Yii::app()->name), '/') . '. ' . Yii::t('main', 'All Rights Reserved.'); ?>
                </div>
            </div>
        </div>
    </footer>

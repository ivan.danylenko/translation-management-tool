<?php
    echo CHtml::tag('p', array(), Yii::t('main', 'The system always messes it all up :/ Here is what it told us this time:'));

    echo CHtml::tag('p', array(), TbHtml::quote(TbHtml::b($error['code'] . ', ' . $error['message'])));

    echo CHtml::openTag('p');
        echo Yii::t('main', 'If you keep on seeing this problem - please ');

        echo TbHtml::link(
            Yii::t('main', 'contact us'),
            array('site/contact', 'error' => $error['code'] . ', ' . $error['message'])
        );
    echo CHtml::closeTag('p');

    if (Yii::app()->request->urlReferrer) {
        echo TbHtml::linkButton(
            Yii::t('main', 'Go back'),
            array(
                'url' => Yii::app()->request->urlReferrer,
                'color' =>TbHtml::BUTTON_COLOR_PRIMARY,
            )
        );
    }
<?php
    echo TbHtml::linkButton(
        Yii::t('main', 'Sign up'),
        array(
            'url' => array('#'),
            'color' =>TbHtml::BUTTON_COLOR_SUCCESS,
            'size' => TbHtml::BUTTON_SIZE_XLARGE,
            'class' => 'text-uppercase',
        )
    );
    echo TbHtml::linkButton(
        Yii::t('main', 'Login'),
        array(
            'url' => array('site/login'),
            'color' =>TbHtml::BUTTON_COLOR_DEFAULT,
            'size' => TbHtml::BUTTON_SIZE_XLARGE,
            'class' => 'text-uppercase',
        )
    );
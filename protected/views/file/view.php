<?php
    $this->pageTitle = $model->project->name . ' > ' . $model->name;

    $this->breadcrumbs=array(
        Yii::t('main', 'Projects')=>array('project/index'),
        $model->project->name=>array('project/' . $model->project->id . '/view'),
        $model->name,
    );

    echo TbHtml::pageHeader($model->name, $model->project->name . ' (' . $model->version . '#' . Yii::app()->utils->formatSystemVersion($model->system_version) . ')');

    echo CHtml::tag(
        'div',
        array(
            'class' => 'text-right action-buttons'
        ),
        TbHtml::buttonGroup(array(
            array(
                'label' => TbHtml::icon('glyphicon-duplicate'),
                'url' => array('file/' . $model->id . '/copy'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Copy file'),
                )
            ),
            array(
                'label' => TbHtml::icon('glyphicon-time'),
                'url' => array('file/' . $model->id . '/history'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'File history'),
                )
            ),
            array(
                'label' => TbHtml::icon(TbHtml::ICON_PENCIL),
                'url' => array('file/' . $model->id . '/update'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Update file'),
                )
            ),
            array(
                'label' => TbHtml::icon(TbHtml::ICON_TRASH),
                'url' => array('file/' . $model->id . '/delete'),
                'color' => TbHtml::BUTTON_COLOR_WARNING,
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Delete file'),
                )
            ),
        ))
    ); ?>

    <table class="hidden-xs hidden-sm hidden-md hidden-lg hidden-xl">
        <tbody>
            <?php
                echo CHtml::openTag('tr', array('id' => 'empty-translations-row', 'class' => 'success'));
                    if ($model->key_column == '1') {
                        Yii::app()->translationTableGenerator->renderCell('key', '__NEW_ROW__', '');
                    }

                    foreach ($model->languages as $language) {
                        Yii::app()->translationTableGenerator->renderCell($language->id, '__NEW_ROW__', '', true);
                    }

                    echo CHtml::openTag('td', array( 'class' => 'translation-action-cell'));
                        echo TbHtml::buttonDropdown(
                            '',
                            array(
                                array(
                                    'label' => Yii::t('main', 'Remove this row'),
                                    'url' => array('#'),
                                    'class' => 'button-remove',
                                    'data-id' => '__NEW_ROW__'
                                ),
                                array(
                                    'label' => Yii::t('main', 'Keep this row'),
                                    'url' => array('#'),
                                    'class' => 'button-keep hidden',
                                    'data-id' => '__NEW_ROW__'
                                )
                            ), array(
                                'menuOptions' => array(
                                    'pull' => TbHtml::PULL_RIGHT,
                                ),
                                'icon' => 'option-vertical',
                                'toggle' => false
                            )
                        );
                    echo CHtml::closeTag('td');
                echo CHtml::closeTag('tr');
            ?>
        </tbody>
    </table>

    <form method="POST" autocomplete="off">
        <table class="table translation-table">
            <thead>
                <tr>
                    <?php
                        if ($model->key_column == '1') {
                            echo CHtml::tag('th', array(), Yii::t('main', 'Key'));
                        }
                        foreach ($model->languages as $language) {
                            echo CHtml::tag('th', array(), $language->name . CHtml::tag('small', array(), ' (' . $language->english_name . ')'));
                        }
                        echo CHtml::tag('th', array(), '');
                    ?>
                </tr>
            </thead>
            <tbody id="translations-rows">
                <?php if (count($translationData)) {
                    foreach ($translationData as $translationId => $translationDataItem) {
                        echo CHtml::openTag('tr', array('id' => 'translation-key-' . $translationId));
                            if ($model->key_column == '1') {
                                if (isset($translationDataItem['key'])) {
                                    Yii::app()->translationTableGenerator->renderCell('key', $translationId, $translationDataItem['key']);
                                } else {
                                    Yii::app()->translationTableGenerator->renderCell('key', $translationId, '');
                                }
                            }

                            foreach ($model->languages as $language) {
                                $translation = isset($translationDataItem[$language->id]['value']) ? $translationDataItem[$language->id]['value'] : '';
                                // if it is a new item then set it as needs translation
                                $needsTranslation = isset($translationDataItem[$language->id]['needs_translation']) ? $translationDataItem[$language->id]['needs_translation'] : '1';
                                Yii::app()->translationTableGenerator->renderCell($language->id, $translationId, $translation, $needsTranslation);
                            }
                            echo CHtml::openTag('td', array( 'class' => 'translation-action-cell'));
                                echo TbHtml::buttonDropdown(
                                    '',
                                    array(
                                        array(
                                            'label' => Yii::t('main', 'View translation history'),
                                            'url' => array('file/' . $model->id . '/translationHistory/' . $translationId),
                                            'linkOptions' => array(
                                                'target' => '_blank'
                                            )
                                        ),
                                        array(
                                            'label' => Yii::t('main', 'Remove this row'),
                                            'url' => array('#'),
                                            'class' => 'button-remove',
                                            'data-id' => $translationId
                                        ),
                                        array(
                                            'label' => Yii::t('main', 'Keep this row'),
                                            'url' => array('#'),
                                            'class' => 'button-keep hidden',
                                            'data-id' => $translationId
                                        )
                                    ), array(
                                        'menuOptions' => array(
                                            'pull' => TbHtml::PULL_RIGHT,
                                        ),
                                        'icon' => 'option-vertical',
                                        'toggle' => false
                                    )
                                );
                            echo CHtml::closeTag('td');
                        echo CHtml::closeTag('tr');
                    }
                } else {
                    echo CHtml::openTag('tr', array('class' => 'no-translations-row'));
                        echo CHtml::tag('td', array(
                            'colspan' => count($model->languages) + 1,
                            'class' => 'text-center',
                        ), Yii::t('main', 'No translations yet'));
                    echo CHtml::closeTag('tr');
                } ?>
            </tbody>
        </table>


        <?php
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::tag(
                    'div',
                    array(
                        'class' => 'col-md-4 text-left-md'
                    ),
                    TbHtml::button(Yii::t('main', 'Add row'), array('id' => 'translations-add-row'))
                );
                echo CHtml::tag(
                    'div',
                    array(
                        'class' => 'col-md-4 text-center-md'
                    ),
                    'Rows added: ' . CHtml::tag(
                        'b',
                        array(
                            'id' => 'new-rows-counter',
                        ),
                        '0'
                    )
                );
                echo CHtml::tag(
                    'div',
                    array(
                        'class' => 'col-md-4 text-right-md'
                    ),
                    TbHtml::submitButton(Yii::t('main', 'Save'))
                );
            echo CHtml::closeTag('div');

            echo CHtml::tag('div', array('id' => 'removed-keys-list'), '');
        ?>
    </form>
<?php
    $this->pageTitle = $model->project->name . ' > ' . $model->name;

    $this->breadcrumbs=array(
        Yii::t('main', 'Projects')=>array('project/index'),
        $model->project->name=>array('project/' . $model->project->id . '/view'),
        $model->name=>array('file/' . $model->id . '/view'),
        Yii::t('main', 'Copy file'),
    );

    $fromVersions = new stdClass();
    $fromVersions->{$model->system_version} = Yii::t('main', 'Current');

    echo TbHtml::pageHeader(Yii::t('main', 'Copy file'), $model->name);

    echo TbHtml::beginFormTb('', Yii::app()->createAbsoluteUrl(Yii::app()->request->getPathInfo()), 'get');
        echo CHtml::openTag('div', array('class' => 'form-group'));
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::label(Yii::t('main', 'Version to copy'), 'selector-version-from'));
            echo CHtml::closeTag('div');
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::openTag('div', array('class' => 'col-md-4'));
                    echo TbHtml::dropDownList(
                        'version',
                        Yii::app()->request->getQuery('version'),
                        array_merge(
                            (array)$fromVersions,
                            $versions
                        ),
                        array(
                            'id' => 'selector-version-from',
                        )
                    );
                echo CHtml::closeTag('div');
                echo CHtml::openTag('div', array('class' => 'col-md-2'));
                    echo TbHtml::formActions(array(
                        TbHtml::submitButton(Yii::t('main', 'Select'), array('color' => TbHtml::BUTTON_COLOR_INFO, 'name' => '')),
                    ));
                echo CHtml::closeTag('div');
            echo CHtml::closeTag('div');
        echo CHtml::closeTag('div');
    echo TbHtml::endForm();

    if (isset($translationData)) { ?>

<form method="POST" autocomplete="off">
    <table class="table translation-table copy-translation-table">
        <thead>
            <tr>
                <?php
                    if ($model->key_column == '1') {
                        echo CHtml::tag('th', array(), Yii::t('main', 'Key'));
                    }
                    foreach ($model->languages as $language) {
                        echo CHtml::tag('th', array(), $language->english_name);
                    }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php if (count($translationData)) {
                foreach ($translationData as $key => $translationDataItem) {
                    echo CHtml::openTag('tr', array('id' => 'translation-key-' . $key));
                        if ($model->key_column == '1') {
                            if (isset($translationDataItem['key'])) {
                                Yii::app()->translationTableGenerator->renderPreviewCell('key', $key, $translationDataItem['key']);
                            } else {
                                Yii::app()->translationTableGenerator->renderPreviewCell('key', $key, '');
                            }
                        }

                        foreach ($model->languages as $language) {
                            $translation = isset($translationDataItem[$language->id]) ? $translationDataItem[$language->id] : '';
                            Yii::app()->translationTableGenerator->renderPreviewCell($language->id, $key, $translation);
                        }
                    echo CHtml::closeTag('tr');
                }
            } else {
                echo CHtml::openTag('tr', array('class' => 'no-translations-row'));
                    echo CHtml::tag('td', array(
                        'colspan' => count($model->languages) + 1,
                        'class' => 'text-center',
                    ), Yii::t('main', 'No translations yet'));
                echo CHtml::closeTag('tr');
            } ?>
        </tbody>
    </table>
    <?php
        echo CHtml::openTag('div', array('class' => 'row'));
            echo CHtml::tag('div', array('class' => 'col-md-12'), CHtml::tag('h3', array(), Yii::t('main', 'The new file data:')));
        echo CHtml::closeTag('div');

        echo CHtml::openTag('div', array('class' => 'row'));
            echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::activeTextFieldControlGroup($newModel, 'name'));
            echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::activeTextFieldControlGroup($newModel, 'comment'));
            echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::activeTextFieldControlGroup($newModel, 'version'));
        echo CHtml::closeTag('div');
        echo CHtml::openTag('div', array('class' => 'row'));
            echo CHtml::tag(
                'div',
                array(
                    'class' => 'col-md-12 text-right-md'
                ),
                TbHtml::submitButton(Yii::t('main', 'Copy'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
            );
        echo CHtml::closeTag('div');
    ?>
</form>
<?php } ?>

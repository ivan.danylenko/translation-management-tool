<div class="row">
    <div class="col-md-12">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
            <fieldset>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <?php
                            echo $form->textFieldControlGroup($model, 'name');
                            echo $form->textareaControlGroup($model, 'comment');
                            echo $form->textFieldControlGroup($model, 'version', array(
                                'append' => '#' . Yii::app()->utils->formatSystemVersion($model->system_version),
                                'help' => Yii::t('main', 'The number at the end is a value that is automatically increased each time the file translations are saved. You can use it to avoid changing the version manually after each change.'),
                            ));
                            echo $form->checkboxControlGroup($model, 'key_column', array(
                                'help' => Yii::t('main', 'If you have custom keys for your translations you can have a separate column for them'),
                            ));
                        ?>
                    </div>

                    <div class="col-md-4">
                        <legend>Languages</legend>
                        <?php
                            foreach ($languages as $language) {
                                echo TbHtml::checkboxControlGroup('Language[' . $language->id . ']', in_array($language->id, $model->languagesList), array(
                                    'label' => $language->name . ' (' . $language->english_name . ')',
                                ));
                            }
                            if ($model->hasErrors('languagesList')) {
                                echo CHtml::tag('div', array('class' => 'has-error'),
                                    CHtml::tag('span', array('class' => 'help-block'), $model->getError('languagesList'))
                                );
                            }
                        ?>
                        <?php echo CHtml::link(Yii::t('main', 'Add new language'), array('language/create')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <?php echo TbHtml::formActions(array(
                            TbHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                        )); ?>
                    </div>
                </div>
            </fieldset>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->
<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Projects')=>array('project/index'),
		$model->project->name=>array('project/' . $model->project->id . '/view'),
		$model->name=>array('file/' . $model->id . '/view'),
		Yii::t('main', 'Update file'),
	);

	echo TbHtml::pageHeader($model->project->name . '/' . $model->name, Yii::t('main', 'Update'));

	$this->renderPartial('_form', array('model' => $model, 'languages' => $languages));
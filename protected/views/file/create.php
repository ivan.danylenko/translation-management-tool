<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Projects')=>array('project/index'),
		$model->project->name=>array('project/' . $model->project->id . '/view'),
		Yii::t('main', 'Create file'),
	);

	echo TbHtml::pageHeader($model->project->name, Yii::t('main', 'Create file'));

	$this->renderPartial('_form', array('model' => $model, 'languages' => $languages));
<?php
    $this->pageTitle = $model->project->name . ' > ' . $model->name;

    $this->breadcrumbs=array(
        Yii::t('main', 'Projects')=>array('project/index'),
        $model->project->name=>array('project/' . $model->project->id . '/view'),
        $model->name=>array('file/' . $model->id . '/view'),
        Yii::t('main', 'Translation history'),
    );

    echo TbHtml::pageHeader(Yii::t('main', 'Translation history'), $model->name);

    echo TbHtml::beginFormTb('', Yii::app()->createAbsoluteUrl(Yii::app()->request->getPathInfo()), 'get');
        echo CHtml::openTag('div', array('class' => 'form-group'));
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::label(Yii::t('main', 'Choose language'), 'language'));
            echo CHtml::closeTag('div');
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::openTag('div', array('class' => 'col-md-4'));
                    echo TbHtml::dropDownList(
                        'language',
                        Yii::app()->request->getQuery('language'),
                        CHtml::listData($model->languages, 'id', 'english_name'),
                        array(
                            'id' => 'language',
                            'empty' => Yii::t('main', 'Select a language')
                        )
                    );
                echo CHtml::closeTag('div');
                echo CHtml::openTag('div', array('class' => 'col-md-2'));
                    echo TbHtml::formActions(array(
                        TbHtml::submitButton(Yii::t('main', 'Select'), array('color' => TbHtml::BUTTON_COLOR_SUCCESS, 'name' => '')),
                    ));
                echo CHtml::closeTag('div');
            echo CHtml::closeTag('div');
        echo CHtml::closeTag('div');
    echo TbHtml::endForm();

    if (isset($translationsHistory) && count($translationsHistory)) {
        echo CHtml::openTag('div', array('class' => 'translation-history-container'));
            foreach ($translationsHistory as $key => $translationsHistoryItem) {
                // if ($translationsHistoryItem->value_before != $translationsHistoryItem->value_after) {
                    echo CHtml::openTag('div', array('class' => 'translation-history-item'));
                        echo CHtml::openTag('div', array('class' => 'translation-history-data bg-primary col-md-12'));
                            echo CHtml::encode($translationsHistoryItem->value_after);
                            echo CHtml::openTag('div', array('class' => 'translation-history-info'));
                                echo sprintf('<b>%s</b> (v. %s#%05d)', Yii::app()->dateTimeFormatter->toFormattedDateTime($translationsHistoryItem->created_date), $translationsHistoryItem->version, $translationsHistoryItem->system_version);
                            echo CHtml::closeTag('div');
                        echo CHtml::closeTag('div');
                    echo CHtml::closeTag('div');
                // }
            }
        echo CHtml::closeTag('div');
    } else {
        echo CHtml::openTag('div', array('class' => 'row'));
            echo CHtml::openTag('div', array('class' => 'col-md-12'));
                if ($language === null || strlen($language) == 0) {
                    echo Yii::t('main', 'Please, select a language');
                } else {
                    echo Yii::t('main', 'Nothing happened to this translation');
                }
            echo CHtml::closeTag('div');
        echo CHtml::closeTag('div');
    }

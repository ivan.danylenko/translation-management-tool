<?php
    $this->pageTitle = $model->project->name . ' > ' . $model->name;

    $this->breadcrumbs = array(
        Yii::t('main', 'Projects')=>array('project/index'),
        $model->project->name=>array('project/' . $model->project->id . '/view'),
        $model->name=>array('file/' . $model->id . '/view'),
        Yii::t('main', 'File history'),
    );

    $fromVersions = new stdClass();
    $fromVersions->{$model->system_version} = Yii::t('main', 'Current');

    echo TbHtml::pageHeader(Yii::t('main', 'File history'), $model->name);

    echo TbHtml::beginFormTb('', Yii::app()->createAbsoluteUrl(Yii::app()->request->getPathInfo()), 'get');
        echo CHtml::openTag('div', array('class' => 'form-group'));
            echo CHtml::openTag('div', array('class' => 'row'));
                echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::label(Yii::t('main', 'The later one'), 'selector-version-from'));
                echo CHtml::tag('div', array('class' => 'col-md-4'), TbHtml::label(Yii::t('main', 'The earlier one'), 'selector-version-to'));
            echo CHtml::closeTag('div');
            echo CHtml::openTag('div', array('class' => 'row'));
                echo TbHtml::hiddenField('id', $model->id);
                echo CHtml::openTag('div', array('class' => 'col-md-4'));
                    echo TbHtml::dropDownList(
                        'from_version',
                        Yii::app()->request->getQuery('from_version'),
                        array_merge(
                            (array)$fromVersions,
                            $versions
                        ),
                        array(
                            'id' => 'selector-version-from',
                        )
                    );
                echo CHtml::closeTag('div');
                echo CHtml::openTag('div', array('class' => 'col-md-4'));
                    echo TbHtml::dropDownList(
                        'to_version',
                        Yii::app()->request->getQuery('to_version'),
                        $versions,
                        array(
                            'id' => 'selector-version-to',
                        )
                    );
                echo CHtml::closeTag('div');
                echo CHtml::openTag('div', array('class' => 'col-md-2'));
                    echo TbHtml::checkBoxControlGroup(
                        'hide_unchanged_rows',
                        (Yii::app()->request->getQuery('hide_unchanged_rows', '0') == '1'),
                        array(
                            'label' => Yii::t('main', 'Hide unchanged rows'),
                        )
                    );
                echo CHtml::closeTag('div');
                echo CHtml::openTag('div', array('class' => 'col-md-2'));
                    echo TbHtml::formActions(array(
                        TbHtml::submitButton(Yii::t('main', 'Compare'), array('color' => TbHtml::BUTTON_COLOR_INFO, 'name' => '')),
                    ));
                echo CHtml::closeTag('div');
            echo CHtml::closeTag('div');
        echo CHtml::closeTag('div');
    echo TbHtml::endForm();

    if (isset($translationData)) {

        echo TbHtml::openTag('table', array(
            'class' => 'table translation-table diff-translation-table'
        ));
            echo TbHtml::openTag('thead');
                echo TbHtml::openTag('tr');
                    if (Yii::app()->utils->translationDiffHasKey($translationData)) {
                        echo CHtml::tag('th', array(), Yii::t('main', 'Key'));
                    }
                    foreach ($model->languages as $language) {
                        echo CHtml::tag('th', array(), $language->english_name);
                    }
                echo TbHtml::closeTag('tr');
            echo TbHtml::closeTag('thead');
            echo TbHtml::openTag('tbody');
                if (count($translationData)) {
                    foreach ($translationData as $key => $translationDataItem) {
                        echo CHtml::openTag('tr', array('id' => 'translation-key-' . $key, 'data-id' => $key));

                            if (Yii::app()->utils->translationDiffHasKey($translationData)) {
                                if (is_array($translationDataItem['key'])) {
                                    Yii::app()->translationTableGenerator->renderDiffCell($model->id, $key, $translationDataItem['key']['oldValue'], $translationDataItem['key']['newValue'], $translationDataItem['key']['action']);
                                } else {
                                    Yii::app()->translationTableGenerator->renderDiffCell($model->id, $key, $translationDataItem['key']);
                                }
                            }

                            foreach ($model->languages as $language) {
                                $translation = isset($translationDataItem[$language->id]) ? $translationDataItem[$language->id] : '';
                                if (is_array($translation)) {
                                    Yii::app()->translationTableGenerator->renderDiffCell($model->id, $key, $translation['oldValue'], $translation['newValue'], $translation['action']);
                                } else {
                                    Yii::app()->translationTableGenerator->renderDiffCell($model->id, $key, $translation);
                                }
                            }
                        echo CHtml::closeTag('tr');
                    }
                } else {
                    echo CHtml::openTag('tr', array('class' => 'no-translations-row'));
                        echo CHtml::tag('td', array(
                            'colspan' => count($model->languages),
                            'class' => 'text-center',
                        ), Yii::t('main', 'There is no difference between thses versions'));
                    echo CHtml::closeTag('tr');
                }
            echo TbHtml::closeTag('tbody');
        echo TbHtml::closeTag('table');
    }

<?php
    $this->breadcrumbs=array(
        Yii::t('main', 'Languages'),
    );

    echo TbHtml::pageHeader(Yii::t('main', 'Languages'), Yii::t('main', 'List'));

    echo CHtml::tag(
        'div', 
        array(
            'class' => 'text-right action-buttons'
        ),
        TbHtml::buttonGroup(array(
            array(
                'label' => TbHtml::icon(TbHtml::ICON_PLUS),
                'url' => array('language/create'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Add new language'),
                )
            ),
        ))
    );

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
        'dataProvider' => $model->search(),
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '#',
                'htmlOptions' => array('style' =>'width: 60px'),
            ),
            'name',
            'english_name',
            array(
                'value' => function($data) {

                    echo TbHtml::buttonDropdown(
                        Yii::t('main', 'Options'),
                        array(
                            array('label' => Yii::t('main', 'Edit language data'), 'url' => array('language/' . $data->id . '/update')),
                            array('label' => Yii::t('main', 'Remove language'), 'url' => array('language/' . $data->id . '/delete')),
                        ), array(
                            'menuOptions' => array(
                                'pull' => TbHtml::PULL_RIGHT
                            )
                        )
                    );
                },
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'text-right',
                )
            ),
        )
    ));

<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Languages')=>array('index'),
		Yii::t('main', 'Add'),
	);

	echo TbHtml::pageHeader(Yii::t('main', 'Languages'), Yii::t('main', 'Add'));

	$this->renderPartial('_form', array('model'=>$model));
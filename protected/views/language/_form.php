
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
            <fieldset>
                <?php
                    if ($model->isNewRecord) {
                        echo $form->textFieldControlGroup($model, 'id');
                    }
                    echo $form->textFieldControlGroup($model, 'name');
                    echo $form->textFieldControlGroup($model, 'english_name');
                    echo TbHtml::formActions(array(
                        TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                    ));
                ?>
            </fieldset>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->
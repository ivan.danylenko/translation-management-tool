<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Languages')=>array('index'),
		Yii::t('main', 'Update'),
	);

	echo TbHtml::pageHeader($model->english_name, Yii::t('main', 'Languages'));
	
	$this->renderPartial('_form', array('model'=>$model));
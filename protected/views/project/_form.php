
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
            <fieldset>
                <?php 
                    echo $form->textFieldControlGroup($model, 'name');
                    echo $form->textareaControlGroup($model, 'description');
                    echo TbHtml::formActions(array(
                        TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                    ));
                ?>
            </fieldset>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->
<?php
    $this->pageTitle = Yii::t('main', 'Projects');

    $this->breadcrumbs=array(
        Yii::t('main', 'Projects'),
    );

    echo TbHtml::pageHeader(Yii::t('main', 'Projects'), '');

    echo CHtml::tag(
        'div', 
        array(
            'class' => 'text-right action-buttons'
        ),
        TbHtml::buttonGroup(array(
            array(
                'label' => TbHtml::icon(TbHtml::ICON_PLUS),
                'url' => array('project/create'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Create new project'),
                )
            )
        ))
    );

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
        'dataProvider' => $model->search(),
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '#',
                'htmlOptions' => array('style' =>'width: 60px'),
            ),
            array(
                'name' => 'name',
                'type' => 'raw',
                'value' => function($data) {
                    return TbHtml::link($data->name, array('project/' . $data->id . '/view'));
                },
            ),
            'description',
            array(
                'value' => function($data) {

                    echo TbHtml::buttonDropdown(
                        Yii::t('main', 'Options'),
                        array(
                            array('label' => Yii::t('main', 'Edit project data'), 'url' => array('project/' . $data->id . '/update')),
                            array('label' => Yii::t('main', 'Delete project'), 'url' => array('project/' . $data->id . '/delete')),
                        ), array(
                            'menuOptions' => array(
                                'pull' => TbHtml::PULL_RIGHT
                            )
                        )
                    );
                },
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'text-right',
                )
            ),
        )
    ));

<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Projects')=>array('project/index'),
		$model->name=>array('project/' . $model->id . '/view'),
		Yii::t('main', 'Update project'),
	);

	echo TbHtml::pageHeader($model->name, Yii::t('main', 'Update'));

	$this->renderPartial('_form', array('model'=>$model));
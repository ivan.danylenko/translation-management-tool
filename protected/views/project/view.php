<?php
    $this->pageTitle = $model->name;

    $this->breadcrumbs=array(
        Yii::t('main', 'Projects')=>array('project/index'),
        $model->name,
    );

    echo TbHtml::pageHeader($model->name, Yii::t('main', 'Files'));

    echo CHtml::tag(
        'div', 
        array(
            'class' => 'text-right action-buttons'
        ),
        TbHtml::buttonGroup(array(
            array(
                'label' => TbHtml::icon(TbHtml::ICON_PLUS),
                'url' => array('file/create', 'project_id' => $model->id),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Create new file'),
                )
            ),
            array(
                'label' => TbHtml::icon(TbHtml::ICON_PENCIL),
                'url' => array('project/' . $model->id . '/update'),
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Update project'),
                )
            ),
            array(
                'label' => TbHtml::icon(TbHtml::ICON_TRASH),
                'url' => array('project/' . $model->id . '/delete'),
                'color' => TbHtml::BUTTON_COLOR_WARNING,
                'htmlOptions' => array(
                    'title' => Yii::t('main', 'Delete project'),
                )
            ),
        ))
    );

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
        'dataProvider' => $fileModel->search(),
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '#',
                'htmlOptions' => array('style' =>'width: 60px'),
            ),
            array(
                'name' => 'name',
                'type' => 'raw',
                'value' => function($data) {
                    return TbHtml::link($data->name, array('file/' . $data->id . '/view'));
                },
            ),
            array(
                'header' => Yii::t('main', 'Rows'),
                'value' => '$data->translationsCount',
            ),
            array(
                'header' => Yii::t('main', 'Not translated phrases'),
                'value' => '$data->markedTranslationsCount',
            ),
            array(
                'name' => 'comment',
                'sortable'=>false,
            ),
            'version',
            array(
                'name' => 'system_version',
                'sortable'=>false,
            ),
            array(
                'value' => function($data) {

                    echo TbHtml::buttonDropdown(
                        Yii::t('main', 'Options'),
                        array(
                            array('label' => Yii::t('main', 'Edit translations'), 'url' => array('file/' . $data->id . '/view')),
                            array('label' => Yii::t('main', 'Copy file'), 'url' => array('file/' . $data->id . '/copy')),
                            array('label' => Yii::t('main', 'View file history'), 'url' => array('file/' . $data->id . '/history')),
                            array('label' => Yii::t('main', 'Edit file data'), 'url' => array('file/' . $data->id . '/update')),
                            array('label' => Yii::t('main', 'Delete file'), 'url' => array('file/' . $data->id . '/delete')),
                        ), array(
                            'url' => array('file/' . $data->id . '/diff'),
                            // 'color' => TbHtml::BUTTON_COLOR_INFO,
                            'menuOptions' => array(
                                'pull' => TbHtml::PULL_RIGHT
                            )
                        )
                    );
                },
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'text-right',
                )
            ),
        )
    ));
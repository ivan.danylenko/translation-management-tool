
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/' . 'default.bootstrap.min.css') ); ?>
    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/' . 'landing.css') ); ?>
    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/' . 'main.css') ); ?>
    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css.landing') . '/' . 'animate.css') ); ?>

    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js.landing') . '/modernizr-2.6.2.min.js') ); ?>

    <!--[if lt IE 9]>
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js.landing') . '/respond.min.js') ); ?>
    <!-- FOR IE9 below -->
    <![endif]-->

    </head>
    <body>
    

    <?php echo $content; ?>
    
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js.landing') . '/jquery.min.js') ); ?>
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js') . '/bootstrap.min.js') ); ?>
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js.landing') . '/jquery.waypoints.min.js') ); ?>
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js.landing') . '/main.js') ); ?>
    </body>
</html>

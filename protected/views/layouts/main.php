<?php //Yii::app()->yiistrap->register(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />

    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/' . Yii::app()->params['theme'] .'.bootstrap.min.css') ); ?>
    <?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/main.css') ); ?>

    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js') . '/jquery-1.12.0.min.js') ); ?>
    <?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js') . '/jquery-ui.js') ); ?>

    <title><?php echo CHtml::encode($this->pageTitle) . ' | ' . CHtml::encode(Yii::app()->name); ?></title>
</head>

<body class="theme-<?php echo Yii::app()->params['theme']; ?>">
<?php
    echo TbHtml::tag('div', array('class' => 'container-fluid navbar-top'), $this->widget('UserMenu', array(), true));

    if (count($this->breadcrumbs)) {
        echo TbHtml::breadcrumbs(
            array_merge(
                array(
                    Yii::t('main', Yii::app()->user->company->name) => array('site/index'),
                ),
                $this->breadcrumbs)
        );
    }

    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        $key = strtoupper($key);
        echo TbHtml::alert(constant("TbHtml::ALERT_COLOR_$key"), $message);
    }

    echo TbHtml::tag(
        'div',
        array(
            'id' => 'content',
            'class' => 'container-fluid'
        ),
        $content
    );

    echo TbHtml::tag(
        'div',
        array(
            'class' => 'footer container-fluid text-left'
        ),
        Yii::t('main', 'Copyright') . ' &copy; ' . date('Y') . ' ' . TbHtml::link(CHtml::encode(Yii::app()->name), '/') . ' ' . Yii::t('main', 'All Rights Reserved.')
    );

?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'aboutModal',
    'header' => 'About',
    'content' => CHtml::tag('p', array(), Yii::t('main', 'Go Translating is the modern TRANSLATION MANAGEMENT TOOL. You don\'t have to feel pain every time trying to remember what was that word or sentence.'))
)); ?>


<?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js') . '/custom.js') ); ?>
<?php Yii::app()->clientScript->registerScriptFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.js') . '/bootstrap.min.js') ); ?>
<?php Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.assets.css') . '/main.css') ); ?>
<?php Yii::app()->clientScript->registerCssFile( 'https://fonts.googleapis.com/css?family=Open+Sans' ); ?>
<script>
$( function() {
    $("[title]").tooltip();
} );
</script>
</body>
</html>

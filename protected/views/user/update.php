<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Users'), //=>array('user/index'),
		$model->username, //=>array('view','id'=>$model->id),
		Yii::t('main', 'Update user'),
	);

	echo TbHtml::pageHeader($model->username, Yii::t('main', 'Update'));

	$this->renderPartial('_form', array('model'=>$model));
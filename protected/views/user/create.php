<?php
	$this->breadcrumbs=array(
		Yii::t('main', 'Projects')=>array('project/index'),
		Yii::t('main', 'Create'),
	);

	echo TbHtml::pageHeader(Yii::t('main', 'Projects'), Yii::t('main', 'Create'));

	$this->renderPartial('_form', array('model'=>$model)); ?>
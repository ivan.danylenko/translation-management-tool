<?php

class m160513_154704_create_history_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{translation_history}}', array(
			'id' => 'pk',
			'version' => 'string NOT NULL',
			'system_version' => 'string NOT NULL',
			'file_id' => 'integer NOT NULL',
			'translation_id' => 'integer NOT NULL',
			'language_id' => 'string NOT NULL',
			'action' => 'string NOT NULL',
			'value_before' => 'text NOT NULL',
			'value_after' => 'text NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');


		$insertTranslationTriggerSql = "
		CREATE TRIGGER `INSERT_translation` AFTER INSERT ON `tmt_translation`
			FOR EACH ROW BEGIN
				INSERT INTO `tmt_translation_history`(`version`, `system_version`, `translation_id`, `file_id`, `language_id`, `action`, `value_before`, `value_after`) SELECT `version`, `system_version`, NEW.`id`, NEW.`file_id`, NEW.`language_id`, 'insert', '', NEW.`value` FROM `tmt_file` WHERE id=NEW.`file_id`;
			END;";

		$updateTranslationTriggerSql = "
		CREATE TRIGGER `UPDATE_translation` AFTER UPDATE ON `tmt_translation`
			FOR EACH ROW BEGIN
				IF OLD.value != NEW.value THEN
					INSERT INTO `tmt_translation_history`(`version`, `system_version`, `translation_id`, `file_id`, `language_id`, `action`, `value_before`, `value_after`) SELECT `version`, `system_version`, OLD.`id`, OLD.`file_id`, OLD.`language_id`, 'update', OLD.`value`, NEW.`value` FROM `tmt_file` WHERE id=OLD.`file_id`;
				END IF;
			END;";

		$deleteTranslationTriggerSql = "
		CREATE TRIGGER `DELETE_translation` AFTER DELETE ON `tmt_translation`
			FOR EACH ROW BEGIN
				INSERT INTO `tmt_translation_history`(`version`, `system_version`, `translation_id`, `file_id`, `language_id`, `action`, `value_before`, `value_after`) SELECT `version`, `system_version`, OLD.`id`, OLD.`file_id`, OLD.`language_id`, 'delete', OLD.`value`, '' FROM `tmt_file` WHERE id=OLD.`file_id`;
			END;";

		$this->execute($insertTranslationTriggerSql);
		$this->execute($updateTranslationTriggerSql);
		$this->execute($deleteTranslationTriggerSql);
	}

	public function safeDown()
	{
		$this->dropTable('{{translation_history}}');

		$this->execute("DROP TRIGGER IF EXISTS `INSERT_translation`;");
		$this->execute("DROP TRIGGER IF EXISTS `UPDATE_translation`;");
		$this->execute("DROP TRIGGER IF EXISTS `DELETE_translation`;");
	}
}
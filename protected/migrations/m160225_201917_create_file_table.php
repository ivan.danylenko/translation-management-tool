<?php

class m160225_201917_create_file_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{file}}', array(
			'id' => 'pk',
			'project_id' => 'integer NOT NULL',
			'name' => 'string NOT NULL',
			'comment' => 'text NOT NULL',
			'version' => 'string NOT NULL',
			'key_column' => 'boolean',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
	}

	public function safeDown()
	{
		$this->dropTable('{{file}}');
	}
}
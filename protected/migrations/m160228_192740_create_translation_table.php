<?php

class m160228_192740_create_translation_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{translation}}', array(
			'id' => 'integer NOT NULL',
			'file_id' => 'integer NOT NULL',
			'language_id' => 'string NOT NULL',
			'value' => 'text NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		$this->addPrimaryKey('PRIMARY', '{{translation}}', array('id', 'file_id', 'language_id'));

		$this->alterColumn('{{translation}}', 'id', 'integer NOT NULL AUTO_INCREMENT');
	}

	public function safeDown()
	{
		$this->dropTable('{{translation}}');
	}
}
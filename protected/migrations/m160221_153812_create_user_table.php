<?php

class m160221_153812_create_user_table extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('{{user}}', array(
			'id' => 'pk',
			'username' => 'string NOT NULL',
			'password' => 'string NOT NULL',
			'email' => 'string NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		// admin/123
		$this->insert('{{user}}', array(
			'id' => 1,
			'username' => 'admin',
			'password' => '$2y$13$Z2p208KVZ7Co6cXZqwVLVu9JuQfvTuHicyOJ/N3YCWjFpjhTrc.N.',
			'email' => 'admin@gotranslating.com'
		));
	}

	public function safeDown()
	{
		$this->dropTable('{{user}}');
	}
}
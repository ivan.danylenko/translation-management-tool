<?php

class m160516_115059_add_system_version_field extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{file}}', 'system_version', 'string DEFAULT 0 AFTER `version`');
	}

	public function safeDown()
	{
		$this->dropColumn('{{file}}', 'system_version');
	}
}
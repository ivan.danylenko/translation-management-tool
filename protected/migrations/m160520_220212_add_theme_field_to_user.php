<?php

class m160520_220212_add_theme_field_to_user extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{user}}', 'theme', 'string NOT NULL AFTER `email`');
	}

	public function safeDown()
	{
		$this->dropColumn('{{user}}', 'theme');
	}
}
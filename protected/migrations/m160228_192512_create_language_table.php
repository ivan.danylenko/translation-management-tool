<?php

class m160228_192512_create_language_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{language}}', array(
			'id' => 'string NOT NULL PRIMARY KEY',
			'name' => 'string NOT NULL',
			'english_name' => 'string NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		$this->insert('{{language}}', array(
			'id' => 'en',
			'name' => 'English',
			'english_name' => 'English',
		));
	}

	public function safeDown()
	{
		$this->dropTable('{{language}}');
	}
}
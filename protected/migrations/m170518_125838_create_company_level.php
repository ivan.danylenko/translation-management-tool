<?php

class m170518_125838_create_company_level extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{company}}', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		$this->insert('{{company}}', array(
			'id' => 1,
			'name' => 'System',
		));

		$this->addColumn('{{user}}', 'company_id', 'integer NOT NULL DEFAULT 1 after id');
		$this->addColumn('{{project}}', 'company_id', 'integer NOT NULL DEFAULT 1 after id');
	}

	public function safeDown()
	{
		$this->dropTable('{{company}}');

		$this->dropColumn('{{user}}', 'company_id');
		$this->dropColumn('{{project}}', 'company_id');
	}
}
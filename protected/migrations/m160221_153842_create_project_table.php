<?php

class m160221_153842_create_project_table extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('{{project}}', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'description' => 'text NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
	}

	public function safeDown()
	{
		$this->dropTable('{{project}}');
	}
}
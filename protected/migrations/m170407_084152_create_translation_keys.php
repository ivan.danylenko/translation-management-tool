<?php

class m170407_084152_create_translation_keys extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{translation_key}}', array(
			'file_id' => 'integer NOT NULL',
			'translation_id' => 'integer NOT NULL',
			'key' => 'string NOT NULL',
			'created_date' => 'datetime NOT NULL DEFAULT CURRENT_TIMESTAMP',
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		$this->addPrimaryKey('PRIMARY', '{{translation_key}}', array('file_id', 'translation_id'));
	}

	public function safeDown()
	{
		$this->dropTable('{{translation_key}}');
	}
}
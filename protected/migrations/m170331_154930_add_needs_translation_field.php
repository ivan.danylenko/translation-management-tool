<?php

class m170331_154930_add_needs_translation_field extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{translation}}', 'needs_translation', 'tinyint(1) NOT NULL DEFAULT 0 AFTER `value`');
	}

	public function safeDown()
	{
		$this->dropColumn('{{translation}}', 'needs_translation');
	}
}
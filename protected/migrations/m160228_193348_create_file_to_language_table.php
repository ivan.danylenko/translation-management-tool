<?php

class m160228_193348_create_file_to_language_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{file_to_language}}', array(
			'id' => 'pk',
			'file_id' => 'integer NOT NULL',
			'language_id' => 'string NOT NULL',
			'active' => 'boolean',
			'show' => 'boolean'
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
	}

	public function safeDown()
	{
		$this->dropTable('{{file_to_language}}');
	}
}
<?php

class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionUpdate()
	{
		$model = $this->loadModel(Yii::app()->user->id);

		if (Yii::app()->request->getPost('User') !== null) {
			$model->attributes = Yii::app()->request->getPost('User');
			if($model->save()) {
				Yii::app()->params['theme'] = $model->theme;
				Yii::app()->user->setFlash('success', Yii::t('main', 'Saved'));
			}
		}

		$this->render('update', array(
			'model' => $model
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionChangeTheme()
	{
		if (!Yii::app()->user->isGuest) {
			$theme = Yii::app()->request->getQuery('theme');

			if (in_array($theme, array_keys(Yii::app()->params['availableThemes']))) {
				$user = User::model()->findByPk(Yii::app()->user->id);

				if ($user !== null) {
					$user->theme = $theme;
					$user->save(false);
				}
			}
		}
		if (Yii::app()->request->urlReferrer !== null) {
			$this->redirect(Yii::app()->request->urlReferrer);
		} else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}
}

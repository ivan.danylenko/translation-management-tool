<?php

class SiteController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('logout'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('index', 'login', 'error'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Landing page
	 */
	public function actionIndex()
	{
		$this->layout = 'landing';

		$countPhrases = Translation::model()->count();

		// if user is logged in then there is no need to show landing page
		if (Yii::app()->user->isGuest === false) {
			$this->redirect('/project/index');
		} else {
			$this->render('index', array(
				'countPhrases' => $countPhrases,
			));
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH) {
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");
		}

		$model = new LoginForm();

		// if it is ajax validation request
		if(Yii::app()->request->getPost('ajax') == 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (Yii::app()->request->getPost('LoginForm') !== null) {
			$model->attributes = Yii::app()->request->getPost('LoginForm');
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionError()
	{
	    if ($error = Yii::app()->errorHandler->error) {
			$this->render('error',array('error'=>$error));
	    } else {
			$this->redirect(Yii::app()->homeUrl);
	    }
	}
}

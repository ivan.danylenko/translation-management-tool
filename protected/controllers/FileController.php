<?php

class FileController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create', 'update', 'delete', 'history', 'copy', 'translationHistory'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$model->fetchLanguagesList();

		if (Yii::app()->request->getPost('translations') !== null || Yii::app()->request->getPost('translationsToRemove') !== null) {
			$translationData = Yii::app()->request->getPost('translations', array());
			$translationsToRemove = Yii::app()->request->getPost('translationsToRemove', array());

			if ($model->saveTranslations($translationData) === true) {
				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $translationsToRemove);

				$translationsToRemove = Translation::model()->findAll($criteria);

				foreach ($translationsToRemove as $translationToRemove) {
					$translationToRemove->delete();
				}

				$criteria = new CDbCriteria();
				$criteria->compare('system_version', $model->system_version);

				// if there are any translation history items added then the version should be increased.
				// This check is conducted to avoid gaps in system versions
				if (TranslationHistory::model()->count($criteria)) {
					$model->increaseVersion();
				}

				Yii::app()->user->setFlash('success', Yii::t('main', 'Saved'));

				// overwrite the translation data because needs translation can be missed in POST data if
				// it is turned off and then the button is active, since abscense of needs translation
				// is considered as needs translation
				$translationData = $model->getTranslationsData();
			} else {
				Yii::app()->user->setFlash('danger', Yii::t('main', reset($model->getErrors())[0]));
			}
		} else {
			$translationData = $model->getTranslationsData();
		}

		$this->render('view',array(
			'model'=>$model,
			'translationData' => $translationData,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new File();
		if (Yii::app()->request->getQuery('project_id') !== null) {
			$model->project_id = Yii::app()->request->getQuery('project_id');

			if (Yii::app()->request->getPost('File') !== null) {
				$model->attributes = Yii::app()->request->getPost('File');
				$model->languagesList = array_keys(Yii::app()->request->getPost('Language', array()));
				if ($model->save()) {
					$this->redirect(array('file/' . $model->id . '/view'));
				}
			}

			$languages = Language::model()->findAll();

			$this->render('create',array(
				'model'=>$model,
				'languages' => $languages,
			));
		} else {
			$this->redirect(array('project/index'));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$model->fetchLanguagesList();

		if (Yii::app()->request->getPost('File') !== null) {
			$model->attributes = Yii::app()->request->getPost('File');
			$model->languagesList = array_keys(Yii::app()->request->getPost('Language', array()));

			if($model->save()) {
				$this->redirect(array('file/' . $model->id . '/view'));
			}
		}

		$languages = Language::model()->findAll();

		$this->render('update',array(
			'model'=>$model,
			'languages' => $languages,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		$this->redirect(array('project/' . $model->project_id . '/view'));
	}

	public function actionIndex()
	{
		$this->redirect(array('project/index'));
	}

	/**
	 * Shows difference between versions of a file.
	 */
	public function actionHistory($id)
	{
		$model = $this->loadModel($id);

		$criteria = new CDbCriteria();
		$criteria->select = 'DISTINCT system_version, version, created_date';
		$criteria->compare('file_id', $model->id);
		$criteria->order = 'created_date DESC';
		$translationsHistoryItems = TranslationHistory::model()->findAll($criteria);

		$versions = new stdClass();

		foreach ($translationsHistoryItems as $translationsHistoryItem) {
			$versions->{$translationsHistoryItem->system_version} = sprintf('v. %s#%05d (%s)', $translationsHistoryItem->version, $translationsHistoryItem->system_version, Yii::app()->dateTimeFormatter->toFormattedDateTime($translationsHistoryItem->created_date));
		}

		$versions = (array)$versions;

		if (Yii::app()->request->getQuery('from_version') !== null && Yii::app()->request->getQuery('to_version') !== null) {
			$diffs = $model->getDiff(Yii::app()->request->getQuery('from_version'), Yii::app()->request->getQuery('to_version'), (Yii::app()->request->getQuery('hide_unchanged_rows', '0') == '1'));

			$model->fetchLanguagesList();

			$this->render('diff-viewer',array(
				'model' => $model,
				'versions' => $versions,
				'translationData' => $diffs,
				'from_version' => Yii::app()->request->getQuery('from_version'),
				'to_version' => Yii::app()->request->getQuery('to_version'),
			));
		} else {
			$this->render('diff-viewer',array(
				'model' => $model,
				'versions' => $versions,
			));
		}
	}

	/**
	 * Copy an existing file into a new one
	 */
	public function actionCopy($id)
	{
		$model = $this->loadModel($id);

		$model->fetchLanguagesList();

		$criteria = new CDbCriteria();
		$criteria->select = 'DISTINCT system_version, version, created_date';
		$criteria->compare('file_id', $model->id);
		$criteria->order = 'created_date DESC';
		$translationsHistoryItems = TranslationHistory::model()->findAll($criteria);

		$versions = new stdClass();

		foreach ($translationsHistoryItems as $translationsHistoryItem) {
			$versions->{$translationsHistoryItem->system_version} = sprintf('v. %s#%05d (%s)', $translationsHistoryItem->version, $translationsHistoryItem->system_version, Yii::app()->dateTimeFormatter->toFormattedDateTime($translationsHistoryItem->created_date));
		}

		$versions = (array)$versions;

		if (Yii::app()->request->getQuery('version') !== null) {
			$model->fetchLanguagesList();

			$translationData = $model->getVersionTranslations(Yii::app()->request->getQuery('version'));

			$newModel = new File();
			$newModel->project_id = $model->project_id;
			$newModel->languagesList = $model->languagesList;
			$newModel->name = $model->name . ' - ' . Yii::t('main', 'Copy');

			if (Yii::app()->request->getPost('File') !== null) {
				if (Yii::app()->request->getPost('File') !== null) {
					$newModel->attributes = Yii::app()->request->getPost('File');
					if ($newModel->save() && $newModel->saveTranslations($translationData)) {
						Yii::app()->user->setFlash('success', Yii::t('main', 'Saved'));
						$this->redirect(array('file/' . $newModel->id . '/view'));
					} else {
						Yii::app()->user->setFlash('danger', Yii::t('main', 'An error coccured. Please try again later'));
					}
				}
			}

			$this->render('copy',array(
				'model' => $model,
				'newModel' => $newModel,
				'versions' => $versions,
				'translationData' => $translationData,
				'version' => Yii::app()->request->getQuery('version'),
			));
		} else {
			$this->render('copy',array(
				'model' => $model,
				'versions' => $versions,
			));
		}
	}

	public function actionTranslationHistory($id, $param)
	{
		$model = $this->loadModel($id);
		$model->fetchLanguagesList();

		$translationId = $param;

		if (Yii::app()->request->getQuery('language')) {
			$criteria = new CDbCriteria();
			$criteria->compare('translation_id', $translationId);
			$criteria->compare('file_id', $id);
			$criteria->compare('language_id', Yii::app()->request->getQuery('language'));
			$translationsHistory = TranslationHistory::model()->findAll($criteria);
		} else {
			$translationsHistory = array();
		}


		$this->render('translation-history',array(
			'model' => $model,
			'translationsHistory' => $translationsHistory,
			'language' => Yii::app()->request->getQuery('language')
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return File the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array('project', 'languages', 'fileToLanguages');
		$criteria->compare('project.company_id', Yii::app()->user->company->id);

		$model=File::model()->findByPk($id, $criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}

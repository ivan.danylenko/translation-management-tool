<?php

class AnalyticsModule extends CWebModule
{
	public function init()
	{
		$this->layoutPath = Yii::getPathOfAlias('webroot.protected.views.layouts');
		$this->layout = 'main';

		$this->setImport(array(
			'analytics.models.*',
			'analytics.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}

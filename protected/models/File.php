<?php

/**
 * This is the model class for table "{{file}}".
 *
 * The followings are the available columns in table '{{file}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property string $comment
 * @property string $version
 * @property string $system_version
 * @property string $created_date
 */
class File extends ActiveRecord
{
	const STATUS_INSERT = 'insert';
	const STATUS_UPDATE = 'update';
	const STATUS_DELETE = 'delete';
	public $languagesList = array();

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{file}}';
	}

	public function defaultScope()
	{
		return array(
			'alias' => 'file',
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, name, version', 'required'),
			array('project_id', 'numerical', 'integerOnly'=>true),
			array('name, system_version, version', 'length', 'max'=>255),
			array('key_column', 'boolean'),
			array('languagesList', 'validateLanguages'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_id, name, comment, system_version, version, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'translationsCount' => array(self::STAT, 'Translation', 'file_id', 'select' => 'COUNT(DISTINCT translation.id)'),
			'markedTranslationsCount' => array(self::STAT, 'Translation', 'file_id', 'condition' => 'translation.needs_translation=1'),
			'languages' => array(
				self::MANY_MANY,
				'Language',
				'{{file_to_language}}(file_id, language_id)',
				'order'=> 'CASE WHEN language.id="key" THEN "1"
								WHEN language.id="en" THEN "2"
								ELSE language.id END'
			),
			'fileToLanguages' => array(
				self::HAS_MANY,
				'FileToLanguage',
				'file_id',
				'order'=> 'CASE WHEN fileToLanguages.language_id="key" THEN "1"
								WHEN fileToLanguages.language_id="en" THEN "2"
								ELSE fileToLanguages.language_id END'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'name' => 'Name',
			'comment' => 'Comment',
			'version' => 'Version',
			'system_version' => 'System Version',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->together = true;
		$criteria->with = array('project');

		$criteria->compare('project.company_id', Yii::app()->user->company->id);

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('system_version',$this->system_version,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validateLanguages() {
		if ($this->languagesList === null || !is_array($this->languagesList) || count($this->languagesList) < 2) {
			$this->addError('languagesList', Yii::t('errors', 'Can\'t have less than two languages'));
		}

		if (!in_array('en', $this->languagesList)) {
			$this->addError('languagesList', Yii::t('errors', 'Can\'t remove English'));
		}
	}

	public function afterSave() {
		$existingLanguages = array();

		foreach ($this->fileToLanguages as $fileToLanguage) {
			$existingLanguages[] = $fileToLanguage->language_id;
		}

		$removedLanguages = array_diff($existingLanguages, $this->languagesList);

		foreach ($removedLanguages as $removedLanguage) {
			$criteria = new CDbCriteria();
			$criteria->compare('file_id', $this->id);
			$criteria->compare('language_id', $removedLanguage);
			$fileToLanguage = FileToLanguage::model()->find($criteria);
			$fileToLanguage->delete();
		}

		$addedLanguages = array_diff($this->languagesList, $existingLanguages);

		foreach ($addedLanguages as $addedLanguage) {
			$fileToLanguage = new FileToLanguage();
			$fileToLanguage->file_id = $this->id;
			$fileToLanguage->language_id = $addedLanguage;
			$fileToLanguage->active = '1';
			$fileToLanguage->show = '1';
			$fileToLanguage->save();
		}

		if ($this->key_column == '0') {
			$criteria = new CDbCriteria();
			$criteria->compare('file_id', $this->id);
			$criteria->compare('language_id', 'key');

			Translation::model()->deleteAll($criteria);
		}
	}

	public function afterDelete() {
		$criteria = new CDbCriteria();
		$criteria->compare('file_id', $this->id);

		// delete relations file to languages
		FileToLanguage::model()->deleteAll($criteria);
		// delete file transations
		Translation::model()->deleteAll($criteria);
		// delete file translations history
		TranslationHistory::model()->deleteAll($criteria);
	}

	public function fetchLanguagesList()
	{
		$tempLanguages = array();
		foreach ($this->fileToLanguages as $fileToLanguage) {
			$tempLanguages[] = $fileToLanguage->language_id;
		}

		$this->languagesList = $tempLanguages;
	}

	public function getTranslationsData($includeNeedsTranslation = true)
	{
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array('language', 'translationKey');
		$criteria->compare('translation.file_id', $this->id);

		$translations = Translation::model()->findAll($criteria);

		$translationData = array();

		foreach ($translations as $translation) {
			if (!isset($translationData[$translation->id])) {
				$translationData[$translation->id] = array();
			}
			if ($includeNeedsTranslation === true) {
				$translationData[$translation->id]['key'] = ($translation->translationKey !== NULL) ? $translation->translationKey->key : '';
				$translationData[$translation->id][$translation->language_id] = array(
					'value' => $translation->value,
					'needs_translation' => $translation->needs_translation
				);
			} else {
				$translationData[$translation->id][$translation->language_id] = $translation->value;
			}
		}

		return $translationData;
	}

	public function increaseVersion()
	{
		$this->system_version++;
		$this->save(false);
	}

	public function saveTranslations($translations)
	{
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$needsTranslation = Yii::app()->request->getPost('needs_translation', array());

			foreach ($translations as $translationId => $translationData) {

				$rowHasValues = false;

				// check if row has values by looping through every row value
				foreach ($translationData as $languageId => $columndData) {
					if ($languageId != 'key' && strlen($columndData['value']) > 0) {
						$rowHasValues = true;
						break;
					}
				}

				// the variable is used for
				$newTranslationId = null;

				$key = (isset($translationData['key']) ? $translationData['key'] : '');

				foreach ($translationData as $languageId => $columndData) {
					if ($languageId != 'key') {
						if (strpos($translationId, 'new_row_') === 0) {
							$translation = new Translation();
							if ($newTranslationId !== null) {
								$translation->id = $newTranslationId;
							}
							$translation->file_id = $this->id;
							$translation->language_id = $languageId;
							$translation->value = $columndData['value'];
							$translation->needs_translation = '1';
							$translation->save();
						} else {
							$criteria = new CDbCriteria();
							$criteria->compare('id', $translationId);
							$criteria->compare('file_id', $this->id);
							$criteria->compare('language_id', $languageId);

							$translation = Translation::model()->find($criteria);

							if ($translation === null) {
								$translation = new Translation();
								$translation->id = $translationId;
								$translation->file_id = $this->id;
								$translation->language_id = $languageId;
							}

							$translation->value = $columndData['value'];
							$translation->needs_translation = (isset($columndData['needs_translation']) ? $columndData['needs_translation'] : '0');
						}

						// if there are any values then save, otherwise remove the row
						if ($rowHasValues) {
							$translation->save();
						} else {
							$translation->delete();
						}

						if ($newTranslationId === null) {
							$newTranslationId = $translation->id;
						}
					}
				}

				if ($this->key_column == '1') {
					$criteria = new CDbCriteria();
					$criteria->compare('translation_id', $newTranslationId);

					$translationKey = TranslationKey::model()->find($criteria);

					if ($translationKey === NULL) {
						$translationKey = new TranslationKey();
						$translationKey->file_id = $this->id;
						$translationKey->translation_id = $newTranslationId;
						$translationKey->created_date = date('Y-m-d H:i:s');
					}

					$translationKey->key = $key;
					if (!$translationKey->save()) {
						$this->addError('key_column', reset($translationKey->getErrors())[0]);
						throw new Exception("Validation error", 1);
					}
				}
			}

			$transaction->commit();
			return true;
		} catch (Exception $e) {
			$transaction->rollBack();
			return false;
		}
	}

	private function getChanges($toVersion)
	{
		$criteria = new CDbCriteria();
		$criteria->params = array(
			':toVersion' => (int)$toVersion,
		);
		$criteria->compare('file_id', $this->id);
		$criteria->addCondition('system_version >= :toVersion');
		$criteria->order = 'created_date DESC, translation_id ASC, id DESC';
		$translationHistoryItems = TranslationHistory::model()->findAll($criteria);

		return $translationHistoryItems;
	}

	public function getVersionTranslations($version) {
		$translationHistoryItems = $this->getChanges($version);

		$translationData = $this->getTranslationsData(false);

		foreach ($translationHistoryItems as $translationHistoryItem) {
			$translation_id = (int)$translationHistoryItem->translation_id;
			$language_id = $translationHistoryItem->language_id;

			if ($translationHistoryItem->action == self::STATUS_INSERT) {
				unset($translationData[$translation_id][$language_id]);
			} elseif ($translationHistoryItem->action == self::STATUS_UPDATE) {
				$translationData[$translation_id][$language_id] = $translationHistoryItem->value_before;
			} elseif ($translationHistoryItem->action == self::STATUS_DELETE) {

				if (!isset($translationData[$translation_id])) {
					$translationData[$translation_id] = array();
				}

				if (!isset($translationData[$translation_id][$language_id])) {
					$translationData[$translation_id][$language_id] = $translationHistoryItem->value_before;
				}
			}
		}

		foreach ($translationData as $key => $languages) {
			if (count($languages) == 0) {
				unset($translationData[$key]);
			}
		}

		return $translationData;
	}

	public function getDiff($fromVersion, $toVersion, $hideUnchangedRows)
	{
		$translationHistoryItems = $this->getChanges($toVersion);

		$translationData = $this->getTranslationsData(false);
		$diffedTranslationData = $translationData;

		foreach ($translationHistoryItems as $translationHistoryItem) {
			$translation_id = (int)$translationHistoryItem->translation_id;
			$language_id = $translationHistoryItem->language_id;

			if ($translationHistoryItem->system_version >= $fromVersion) {
				if ($translationHistoryItem->action == self::STATUS_INSERT) {
					unset($translationData[$translation_id][$language_id]);
				} elseif ($translationHistoryItem->action == self::STATUS_UPDATE) {
					$translationData[$translation_id][$language_id] = $translationHistoryItem->value_before;
				} elseif ($translationHistoryItem->action == self::STATUS_DELETE) {

					if (!isset($translationData[$translation_id])) {
						$translationData[$translation_id] = array();
					}

					if (!isset($translationData[$translation_id][$language_id])) {
						$translationData[$translation_id][$language_id] = $translationHistoryItem->value_before;
					}
				}

				// assign the reverted for one step translation array
				$diffedTranslationData = $translationData;
			} else {

				if (!isset($diffedTranslationData[$translation_id])) {
					$diffedTranslationData[$translation_id] = array();
				}

				if (!isset($diffedTranslationData[$translation_id][$language_id])) {
					$diffedTranslationData[$translation_id][$language_id] = array();
				}

				$diffedTranslationData[$translation_id][$language_id] = array(
					'oldValue' => '',
					'newValue' => isset($translationData[$translation_id][$language_id]) ? $translationData[$translation_id][$language_id] : ''
				);

				$diffedTranslationData[$translation_id][$language_id]['action'] = $translationHistoryItem->action;
				if ($translationHistoryItem->action == self::STATUS_INSERT) {
					$diffedTranslationData[$translation_id][$language_id]['oldValue'] = '';
				} elseif ($translationHistoryItem->action == self::STATUS_UPDATE) {
					$diffedTranslationData[$translation_id][$language_id]['oldValue'] = $translationHistoryItem->value_before;
				} elseif ($translationHistoryItem->action == self::STATUS_DELETE) {
					$diffedTranslationData[$translation_id][$language_id]['oldValue'] = $translationHistoryItem->value_before;
					$diffedTranslationData[$translation_id][$language_id]['newValue'] = '';
				}
			}
		}

		// clear empty rows
		foreach ($diffedTranslationData as $id => $languages) {
			if (count($languages) == 0) {
				unset($diffedTranslationData[$id]);
			}
		}

		if ($hideUnchangedRows === true) {
			foreach ($diffedTranslationData as $id => $languages) {
				$rowHasChanges = false;
				foreach ($languages as $language => $data) {
					if (
						(isset($data['oldValue']) && $data['oldValue'] != '') ||
						(isset($data['newValue']) && $data['newValue'] != ''))
					{
						$rowHasChanges = true;
					}
				}

				if ($rowHasChanges === false) {
					unset($diffedTranslationData[$id]);
				}
			}
		}

		// clear rows with no data
		// e.g. when rw was added and then deleted
		foreach ($diffedTranslationData as $id => $languages) {
			$rowHasValues = false;
			foreach ($languages as $language => $data) {
				if (
					(
						(isset($data['oldValue']) && $data['oldValue'] != '') ||
						(isset($data['newValue']) && $data['newValue'] != '')
					)|| (is_string($data)))
				{
					$rowHasValues = true;
				}
			}

			if ($rowHasValues === false) {
				unset($diffedTranslationData[$id]);
			}
		}

		return $diffedTranslationData;
	}
}

<?php

/**
 * This is the model class for table "{{translation_history}}".
 *
 * The followings are the available columns in table '{{translation_history}}':
 * @property integer $id
 * @property string $version
 * @property string $system_version
 * @property integer $translation_id
 * @property integer $file_id
 * @property string $language_id
 * @property string $action
 * @property string $value_before
 * @property string $value_after
 * @property string $created_date
 */
class TranslationHistory extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{translation_history}}';
	}

	public function defaultScope()
	{
		return array(
			'alias' => 'translationHistory',
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('version, system_version, translation_id, file_id, language_id, action, value_before, value_after, created_date', 'required'),
			array('translation_id', 'numerical', 'integerOnly'=>true),
			array('version, system_version, language_id, action', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, version, system_version, translation_id, file_id, language_id, action, value_before, value_after, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'translation' => array(self::BELONGS_TO, 'Translation', 'translation_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'version' => 'Version',
			'system_version' => 'System Version',
			'translation_id' => 'Translation',
			'file_id' => 'File',
			'language_id' => 'Language',
			'action' => 'Action',
			'value_before' => 'Value Before',
			'value_after' => 'Value After',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('system_version',$this->system_version,true);
		$criteria->compare('translation_id',$this->translation_id);
		$criteria->compare('file_id',$this->file_id);
		$criteria->compare('language_id',$this->language_id,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('value_before',$this->value_before,true);
		$criteria->compare('value_after',$this->value_after,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranslationHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

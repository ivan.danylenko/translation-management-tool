<?php
	/**
	 * Different useful functions
	 */
	class Utils {

		public function init()
		{
		}

		/**
		 * Checks if the diff translations have key column
		 * @param  string $diff Diff translations array
		 * @return bool         If the key columns is present
		 */
		public function translationDiffHasKey($diff)
		{
			foreach ($diff as $item) {
				return isset($item['key']);
			}
		}

		/**
		 * Formats system version
		 * @param  string $version Version to format
		 * @return string          Formatted version
		 */
		public function formatSystemVersion($version)
		{
			return sprintf('%05d', $version);
		}

		/**
		 * Formats datetime into user's format
		 * @param  string $datetime Datetime in any of the known formats
		 * @return string           Datetime in user defined format
		 */
		public function formatDateTime($datetime)
		{
			$timestamp = strtotime($datetime);

			return date('Y-m-d H:i:s', $timestamp);
		}
	}
<?php

$items = array();

if (Yii::app()->user->id === null) {
    $items = array(
        array(
            'label' => 'Login',
            'url' => array('site/login'),
        )
    );
} else {
    $items = array(
        array('label' => Yii::t('main', 'Languages'), 'url' => array('/language')),
        array('label' => Yii::t('main', 'Projects'), 'url' => array('/project')),
        array('label' => Yii::t('main', 'About'), 'url' => '#', 'data-toggle' => 'modal', 'data-target' => '#aboutModal'),
        array(
            'label' => Yii::app()->user->name,
            'items' => array(
                array('label' => Yii::t('main', 'Profile'), 'url' => array('user/update')),
                array('label' => Yii::t('main', 'Logout'), 'url' => array('site/logout')),
            )
        )
    );
}

if (in_array(Yii::app()->params['theme'], array('darkly', 'sandstone', 'superhero', 'yeti'))) {
    $logo = CHtml::image('/images/logo-white.png', CHtml::encode(Yii::app()->name));
} else {
    $logo = CHtml::image('/images/logo.png', CHtml::encode(Yii::app()->name));
}

$this->widget('bootstrap.widgets.TbNavbar', array(
    'brandLabel' => $logo,
    'display' => null, // default is static to top
    'fluid' => true,
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbNav',
            'htmlOptions'=>array('class'=>'nav navbar-nav navbar-right'),
            'items' => $items,
        ),
    ),
));

<?php

class NotDeletableActiveRecord extends ActiveRecord {

	public function deleteAll($condition='', $params=array())
	{
		$builder = $this->getCommandBuilder();
		$criteria = $builder->createCriteria($condition, $params);

		$this->updateAll(array(
			'deleted' => '1',
			'deleted_date' => date('Y-m-d H:i:s'),
		), $criteria);

		return true;
	}

	protected function beforeFind() {
		$criteria = new CDbCriteria;
		$criteria->compare($this->getTableAlias() . '.deleted',  '0');
		$this->dbCriteria->mergeWith($criteria);
		parent::beforeFind();
	}

	public function beforeDelete() {
		$this->deleted = '1';
		$this->deleted_date = date('Y-m-d H:i:s');
		$this->save(false);
		return false;
	}
}
<?php

class WebUser extends CWebUser {

	private $_model = null;

	function getTheme() {
		if ($user = $this->getModel()) {
			return $user->theme;
		}
	}

	function getCompany() {
		if ($user = $this->getModel()) {
			return $user->company;
		}
	}

	private function getModel() {
		if (!$this->isGuest && $this->_model === null) {
			$criteria = new CDbCriteria();
			$criteria->together = true;
			$criteria->with = array('company');

			$criteria->compare('user.id', Yii::app()->user->id);

			$this->_model = User::model()->find($criteria);
		}
		return $this->_model;
	}

}

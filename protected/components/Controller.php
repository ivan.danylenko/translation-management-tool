<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='main';

	public $breadcrumbs=array();

	public function init() {
		Yii::app()->params['theme'] = 'default';
		if (Yii::app()->user->isGuest == false && in_array(Yii::app()->user->theme, array_keys(Yii::app()->params['availableThemes']))) {
			Yii::app()->params['theme'] = Yii::app()->user->theme;
		}
	}
}
<?php
	/**
	 * Generates table cells for different translation tables, including tranlations table and diff table
	 */
	class TranslationTableGenerator
	{
		public function init() {

		}

		/**
		 * Renders and outputs tranlsation table cell
		 * @param  string $languageId Language id of the translation
		 * @param  int    $key         The id of the translation in the system
		 * @param  string $value       Value of the translation
		 */
		public function renderCell($languageId, $key, $value, $needsTranslation = '0')
		{
			$class = 'translation translation-' . $languageId;
			if ($key == 1) {
				$class .= ' translation-key';
			}

			$translateContiner = '';
			if (!in_array($languageId, array('en', 'key'))) {
				$checkboxId = 'needs-translation-' . $languageId . '-' . $key;
				$needsTranslationClass = 'glyphicon glyphicon-globe toggle-needs-translation';

				if ($needsTranslation == '1') {
					$needsTranslationClass .= ' active';
				}

				if ($key == '__NEW_ROW__') {
					$html = '';
				} else {
					$html = TbHtml::tag(
						'span',
						array(
							'class' => $needsTranslationClass,
							'title' => Yii::t('main', 'Needs translation'),
							'data-checkbox-id' => $checkboxId
						),
						TbHtml::checkbox('translations[' . $key . '][' . $languageId . '][needs_translation]', ($needsTranslation == '1'), array('id' => $checkboxId))
					) .
					TbHtml::tag(
						'span',
						array('class' => 'glyphicon glyphicon-comment', 'title' => Yii::t('main', 'Comments')),
						' '
					);
				}

				$translateContiner = TbHtml::tag(
					'div',
					array('class' => 'translation-to-translate-container'),
					$html
				);
			}

			if ($languageId == 'key') {
				$textAreaClass = 'translations[' . $key . '][key]';
			} else {
				$textAreaClass = 'translations[' . $key . '][' . $languageId . '][value]';
			}

			echo CHtml::tag(
				'td',
				array(),
				$translateContiner .
				TbHtml::textArea(
					$textAreaClass,
					$value,
					array(
						'class' => $class,
						'data-language-id' => $languageId,
						'data-key' => $key,
						'id' => '',
						'rows' => '1'
					)
				)
			);
		}

		/**
		 * Renders and outputs tranlsation table preview cell
		 * @param  string $languageId Language id of the translation
		 * @param  int    $key         The id of the translation in the system
		 * @param  string $value       Value of the translation
		 */
		public function renderPreviewCell($languageId, $key, $value)
		{
			$class = 'translation translation-' . $languageId;
			if ($key == 1) {
				$class .= ' translation-key';
			}
			echo CHtml::tag(
				'td',
				array(),
				TbHtml::hiddenField(
					'translations[' . $key . '][' . $languageId . ']',
					$value,
					array(
						'class' => $class,
						'data-language-id' => $languageId,
						'data-key' => $key,
						'id' => '',
					)
				) . $value
			);
		}

		/**
		 * Renders and outputs tranlsation diff table cell
		 * @param  string $oldValue The value that has been before the action
		 * @param  string $newValue The value that has been after the action
		 * @param  string $action   The action that took place. Possible variants are insert, update and delete
		 */
		public function renderDiffCell($fileId, $id, $oldValueData, $newValueData = null, $action = null)
		{
			$params = array();
			$value = '';
			// action and newValue are defined then there was some action,
			// else no action took place and we can mark this cell as inactive
			if ($newValueData !== null && $action !== null) {
				if ($action == File::STATUS_INSERT) {
					$value = $newValueData;
					$class = 'success';
				} elseif ($action == File::STATUS_UPDATE) {
					$value = '<strike>' . $oldValueData . '</strike>' . $newValueData;
					$class = 'warning';
				} elseif ($action == File::STATUS_DELETE) {
					$value = '<strike>' . $oldValueData . '</strike>';
					$class = 'danger';
				}

				$params = array(
					'class' => 'action-changed ' . $class,
					'data-old-value' => $oldValueData,
					'data-new-value' => $newValueData,
					'data-action' => $action,
				);
			} else {
				$value = $oldValueData;

				$params = array(
					'class' => 'action-none',
					'data-old-value' => $oldValueData,
				);
			}

			echo CHtml::tag('td', $params, $value);
		}
	}

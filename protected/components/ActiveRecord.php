<?php

class ActiveRecord extends CActiveRecord {

	public function insertMultiple($data) {
		$connection = $this->getDbConnection();

		$transaction = $connection->beginTransaction();
		try {
			$savedNumber = $connection->schema->commandBuilder->createMultipleInsertCommand($this->tableName(), $data)->execute();
			if($savedNumber == count($data)) {
				$transaction->commit();
				return true;
			} else {
				$transaction->rollback();
				$this->addError('general', 'Not all rows were saved');
				return false;
			}
		} catch(Exception $e) {
			$transaction->rollback();
			$this->addError('general', $e);
			return false;
		}
	}
}
<?php
	/**
	 * Converts datetime into different formats
	 */
	class DateTimeFormatter {

		/**
		 * Date and time format to output the datetime in
		 * @var string
		 */
		private $_dateTimeFormat = '';

		public function init()
		{
			$this->_dateTimeFormat = 'Y-m-d H:i';
		}

		/**
		 * Formats datetime into the selected datetime format
		 * @param  string $dateTime Datetime in a parsable by strtotime format
		 * @return string           Datetime in the selected datetime format
		 */
		public function toFormattedDateTime($dateTime)
		{
			return date($this->_dateTimeFormat, strtotime($dateTime));
		}
	}
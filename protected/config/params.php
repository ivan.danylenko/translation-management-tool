<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',
	'availableThemes' => array(
		'default' => 'Bootstrap',
		'journal' => 'Journal',
		'sandstone' => 'Sandstone',
		'yeti' => 'Yeti',
	),
);

<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Go Translating',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'bootstrap.helpers.*',
		'bootstrap.components.TbApi',
		'bootstrap.behaviors.TbWidget',
		'bootstrap.widgets.*',
	),

	'defaultController'=>'site',

	'aliases' => array(
        'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
    ),
    'modules' => array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'akunamatata',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::80'),
		),
		'analytics',
    ),

	// application components
	'components'=>array(
		'translationTableGenerator' => array(
			'class' => 'TranslationTableGenerator'
		),
		'user'=>array(
			'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        'userMenu' => array(
            'class' => 'UserMenu',
        ),
        'dateTimeFormatter' => array(
        	'class' => 'DateTimeFormatter'
        ),
        'utils' => array(
        	'class' => 'Utils'
        ),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=tmt',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'tmt_',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'rules'=>array(
				'gii'=>'gii',
				'gii/<controller:\w+>'=>'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				array(
					'<controller>/<action>',
					'pattern'=>'<controller:\w+>/<id:\w+>/<action:\w+>',
					'verb'=>'GET'
				),
				array(
					'<controller>/<action>',
					'pattern'=>'<controller:\w+>/<id:\w+>/<action:\w+>',
					'verb'=>'POST'
				),
				array(
					'<controller>/<action>',
					'pattern'=>'<controller:\w+>/<id:\w+>/<action:\w+>/<param:\w+>',
					'verb'=>'GET'
				),
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// array(
				// 	'class'=>'CWebLogRoute',
				// ),

				'db' => array(
					'class' => 'CWebLogRoute',
					'categories' => 'system.db.CDbCommand',
					'showInFireBug' => true,
					'ignoreAjaxInFireBug' => false,
					'levels'=>'error, warning, trace, profile, info',
				),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);
